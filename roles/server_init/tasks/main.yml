---
- name: Update and upgrade apt packages
  ansible.builtin.apt:
    upgrade: true
    update_cache: true
    autoremove: true
    autoclean: true

- name: Install basic packages
  ansible.builtin.apt:
    name:
      - sudo
      - curl
      - git
      - make
      - gcc
      - libncurses5-dev
      - install-info
      - apt-transport-https
      - ca-certificates
      - gnupg2
      - rsync
      - software-properties-common
      - python3
      - python3-pip
      - python-is-python3 # Not required for Debian 9
      - unzip
      - tmux # Needed for longer tasks when working on server
      - libpq-dev
      - default-libmysqld-dev # Basically a wrapper around the MariaDB dev package
      - acl # Needed for unpriviledged usage of ansible
    state: present
    update_cache: true

- name: Add yarn, node & docker repositories
  vars:
    distro: "{{ ansible_distribution | lower }}"
    release: "{{ ansible_distribution_release }}"
  block:
    - name: Create keyrings folder
      ansible.builtin.file:
        path: /etc/apt/keyrings
        state: directory
        mode: u=rwx,g=rx,o=rx
    - name: Add apt keys for node
      ansible.builtin.apt_key:
        url: https://deb.nodesource.com/gpgkey/nodesource.gpg.key
        keyring: /etc/apt/keyrings/nodesource.gpg
        state: present
    - name: Add apt keys for yarn
      ansible.builtin.apt_key:
        url: https://dl.yarnpkg.com/{{ distro }}/pubkey.gpg
        keyring: /etc/apt/keyrings/yarnkey.gpg
        state: present
    - name: Add apt keys for docker
      ansible.builtin.apt_key:
        url: https://download.docker.com/linux/{{ distro }}/gpg
        keyring: /etc/apt/keyrings/docker.gpg
        state: present
    - name: Add node repository
      ansible.builtin.apt_repository:
        repo: deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_16.x {{ release }} main
        state: present
    - name: Add yarn repository
      ansible.builtin.apt_repository:
        repo: deb [signed-by=/etc/apt/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/{{ distro }}/ stable main
        state: present
    - name: Add docker repository
      ansible.builtin.apt_repository:
        state: present
        repo: >
          deb [arch=amd64 signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/{{ distro }}  {{ release }} stable

- name: Install basic packages, part two electric bogaloo
  ansible.builtin.apt:
    name:
      - nodejs
      - yarn
      - docker-ce
      - docker-ce-cli
      - containerd.io
      - docker-compose-plugin
    state: present
    update_cache: true

# The version supplied by debian is too old
- name: Install python packages needed by ansible
  ansible.builtin.pip:
    name:
      - docker
      - psycopg2
      - PyMySQL

- name: Setup system files
  ansible.builtin.template:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}/{{ item.dest_name | default(item.src) }}"
    mode: u=rw,g=r,o=r
  with_items:
    - { src: motd, dest: /etc }
    - { src: sudoers, dest: /etc }
    - { src: hostname, dest: /etc }
    - { src: hosts, dest: /etc }

- name: Harden server
  ansible.builtin.include_tasks: harden.yaml

- name: Apply user settings
  ansible.builtin.include_tasks: user.yml
  loop: "{{ users }}"
  loop_control:
    loop_var: user

- name: Set up special group for nginx files
  ansible.builtin.group:
    name: www-data
    system: false
    gid: 82

- name: Set up special user for nginx files
  ansible.builtin.user:
    name: www-data
    create_home: false
    system: false
    group: www-data
    groups: docker
    uid: 82

- name: Setup SSH folder for user root
  ansible.builtin.file:
    path: /root/.ssh
    state: directory
    owner: root
    group: root
    mode: u=rwx

- name: Setup SSH keys for user root
  ansible.builtin.assemble:
    src: "{{ role_path }}/keys"
    dest: /root/.ssh/authorized_keys
    regexp: ^({{ root_ssh_keys }})$
    remote_src: false
    owner: root
    group: root
    mode: u=rw

- name: Make sure the docker volume root has the correct permissions
  ansible.builtin.file:
    path: /docker
    state: directory
    owner: root
    group: docker
    mode: u=rwx,g=rx

- name: Check if initial setup was already ran
  ansible.builtin.stat:
    path: /etc/.system-setup
  register: script_exists

- name: Enable ipv6 and restart the server
  when: not script_exists.stat.exists
  block:
    - name: Run the enable_ipv6 command
      ansible.builtin.shell: . ~/.bashrc && bash -ilc enable_ipv6
      args:
        creates: /etc/.system-setup
      environment:
        PATH: /sbin/:{{ ansible_env.PATH }}
    - name: Restart the server to ensure all setings are set
      ansible.builtin.reboot:
        reboot_timeout: 3600

- name: Signal that the initial setup was successful
  ansible.builtin.copy:
    content: ""
    dest: /etc/.system-setup
    force: false
    mode: u=rw,g=r,o=r
