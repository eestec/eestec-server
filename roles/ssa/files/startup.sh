#!/bin/sh


# Install (any new) packages.
cd ..
composer install --no-dev
cd ./public || exit 1


# Start the service
php-fpm
