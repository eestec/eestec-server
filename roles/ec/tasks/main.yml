---
- name: Create DNS entries
  block:
    - name: Create DNS A entry
      community.general.cloudflare_dns:
        zone: "{{ domain }}"
        record: "{{ ec_service_name }}"
        type: A
        value: "{{ ipv4address }}"
        api_token: "{{ cloudflare_api_token }}"
        state: present
    - name: Create DNS AAAA entry
      community.general.cloudflare_dns:
        zone: "{{ domain }}"
        record: "{{ ec_service_name }}"
        type: AAAA
        value: "{{ ipv6address }}"
        api_token: "{{ cloudflare_api_token }}"
        state: present
    - name: Create DNS CNAME entry for ec subdomain
      community.general.cloudflare_dns:
        zone: "{{ domain }}"
        record: "{{ ec_second_service_name }}"
        type: CNAME
        value: " {{ ec_domain }} "
        api_token: "{{ cloudflare_api_token }}"
        state: present

- name: Make sure all necessary paths exist
  ansible.builtin.file:
    path: "{{ item.path }}"
    state: directory
    mode: u=rwx,g=rx,o=rx
  with_items:
    - { path: /home/docker/nginx/assets/sites-enabled }

- name: Make sure all necessary paths exist
  ansible.builtin.file:
    path: "{{ item.path }}"
    state: directory
    mode: u=rwx,g=rx,o=rx
    owner: "{{ item.owner }}"
    group: "{{ item.group }}"
  with_items:
    - { path: /docker/nginx/ec, owner: www-data, group: www-data }

- name: Make sure the website code is up to date
  become: true
  become_user: www-data
  ansible.builtin.git:
    repo: https://gitlab.com/eestec/eestech-challenge-wordpress.git
    dest: /docker/nginx/ec
    force: true
    version: main

- name: Copy Dockerfile
  ansible.builtin.copy:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}/{{ item.dest_name | default(item.src) }}"
    mode: u=rw,g=rw,o=rw
  with_items:
    - { src: Dockerfile, dest: /home/docker/ec }

- name: Copy all necessary files to the host
  ansible.builtin.template:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}/{{ item.dest_name | default(item.src) }}"
    mode: u=rw,g=rw,o=rw
  with_items:
    - { src: wp-config.php, dest: /docker/nginx/ec, owner: www-data, group: www-data }
    - { src: nginx.conf, dest: /home/docker/nginx/assets/sites-enabled, dest_name: "{{ ec_domain }}" }

- name: Create the necessary things for the database
  block:
    - name: Create the MySQL database
      community.mysql.mysql_db:
        name: "{{ ec_db_name }}"
        state: present
        login_host: 127.0.0.1
        login_user: "{{ mysql_db_admin }}"
        login_password: "{{ mysql_db_admin_password }}"
    - name: Create the MySQL user
      community.mysql.mysql_user:
        name: "{{ ec_db_user }}"
        password: "{{ ec_db_password }}"
        state: present
        column_case_sensitive: false
        priv:
          "{{ ec_db_name }}.*": ALL
        login_host: 127.0.0.1
        login_user: "{{ mysql_db_admin }}"
        login_password: "{{ mysql_db_admin_password }}"

- name: Build and start the container
  block:
    - name: Build the container
      community.docker.docker_image:
        name: ec
        build:
          dockerfile: Dockerfile
          path: /home/docker/ec
        source: build
        force_source: true

    - name: Start the container
      community.docker.docker_container:
        name: ec
        image: ec
        hostname: ec.docker
        state: started
        restart: true
        restart_policy: unless-stopped
        networks:
          - name: eestec
            aliases:
              - ec.docker
        # It is VITAL that the absolute path to the blog (/docker/ec)
        # will be the same in the nginx container as well as in the ec
        # container, otherwise it won't work and there doesn't seem to be a way
        # to fix it.
        volumes:
          - /docker/nginx/ec:/docker/ec
