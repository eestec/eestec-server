---
- name: Create DNS entries
  block:
    - name: Create DNS A entry
      community.general.cloudflare_dns:
        zone: "{{ domain }}"
        record: "{{ eestecnet_be_service_name }}"
        type: A
        value: "{{ ipv4address }}"
        api_token: "{{ cloudflare_api_token }}"
        state: present
    - name: Create DNS AAAA entry
      community.general.cloudflare_dns:
        zone: "{{ domain }}"
        record: "{{ eestecnet_be_service_name }}"
        type: AAAA
        value: "{{ ipv6address }}"
        api_token: "{{ cloudflare_api_token }}"
        state: present

- name: Make sure all necessary paths exist
  ansible.builtin.file:
    path: "{{ item.path }}"
    state: directory
    mode: u=rwx,g=rx,o=rx
  with_items:
    - { path: /home/docker/new_eestecnet_BE/ }
    - { path: /home/docker/nginx/assets/sites-enabled }
    - { path: /docker/new_eestecnet_BE/ }

- name: Make sure all necessary paths exist with correct ownership
  ansible.builtin.file:
    path: "{{ item.path }}"
    state: directory
    owner: "{{ item.owner }}"
    group: "{{ item.group }}"
    mode: u=rwx,g=rx,o=rx
  with_items:
    - { path: /docker/nginx/new_eestecnet_BE, owner: www-data, group: www-data }

- name: Make sure the website code is up to date
  become: true
  become_user: www-data
  ansible.builtin.git:
    repo: https://{{ backend_git_user | urlencode }}:{{ backend_git_pwd | urlencode }}@gitlab.com/eestec/eestec.net-backend.git
    dest: /docker/nginx/new_eestecnet_BE
    version: "{{ repository_branch }}"

- name: Copy all necessary templates to the host
  ansible.builtin.template:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}/{{ item.dest_name | default(item.src) }}"
    owner: "{{ item.owner }}"
    group: "{{ item.group }}"
    mode: u=rw,g=r,o=r
  with_items:
    - { src: nginx.conf, dest: /home/docker/nginx/assets/sites-enabled, dest_name: "{{ eestecnet_be_domain }}", owner: root, group: root }
    - { src: .env, dest: /docker/nginx/new_eestecnet_BE, owner: www-data, group: www-data }

- name: Copy all necessary files to the host
  ansible.builtin.copy:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}/{{ item.dest_name | default(item.src) }}"
    mode: "{{ item.mode | default('u=rw,g=r,o=r') }}"
  with_items:
    - { src: Dockerfile, dest: /docker/new_eestecnet_BE }
    - { src: Dockerfile.cron, dest: /docker/new_eestecnet_BE }
    - { src: Dockerfile.queues, dest: /docker/new_eestecnet_BE }
    - { src: php.ini, dest: /docker/nginx/new_eestecnet_BE }
    - { src: policy.xml, dest: /docker/nginx/new_eestecnet_BE }
    - { src: laravel.crontab, dest: /docker/new_eestecnet_BE }
    - { src: supervisord.conf, dest: /docker/new_eestecnet_BE }
    - { src: startup.sh, dest: /docker/nginx/new_eestecnet_BE, mode: "u=rwx,g=r,o=r" }

- name: Create the necessary things for the database
  block:
    - name: Create the postgres database
      community.postgresql.postgresql_db:
        name: "{{ eestecnet_db_name }}"
        state: present
        login_host: 127.0.0.1
        login_user: "{{ postgres_user }}"
        login_password: "{{ postgres_db_password }}"
    - name: Create the postgres user
      community.postgresql.postgresql_user:
        name: "{{ eestecnet_db_user }}"
        password: "{{ eestecnet_db_password }}"
        expires: infinity
        login_host: 127.0.0.1
        login_user: "{{ postgres_user }}"
        login_password: "{{ postgres_db_password }}"
    - name: GRANT ALL PRIVILEGES ON DATABASE eestecnet_db TO eestecnet_user
      community.postgresql.postgresql_privs:
        db: postgres
        privs: ALL
        type: database
        obj: "{{ eestecnet_db_name }}"
        role: "{{ eestecnet_db_user }}"
        login_host: 127.0.0.1
        login_user: "{{ postgres_user }}"
        login_password: "{{ postgres_db_password }}"

- name: Build and start the php-fpm container
  block:
    - name: Ensure latest image is pulled
      community.docker.docker_image_pull:
        name: php:8.2-fpm
    - name: Build the php-fpm container
      community.docker.docker_image:
        name: new-eestecnet-backend
        build:
          dockerfile: /docker/new_eestecnet_BE/Dockerfile
          path: /docker/nginx/new_eestecnet_BE
        source: build
        # Always build (it's cached anyway)
        force_source: true

    - name: Start the php-fpm container
      community.docker.docker_container:
        name: new-eestecnet-backend
        image: new-eestecnet-backend
        hostname: backend.docker
        state: started
        restart: true
        restart_policy: unless-stopped
        networks:
          - name: eestec
            aliases:
              - backend.docker
        volumes:
          - /docker/nginx/new_eestecnet_BE/:/docker/new_eestecnet_BE

- name: Build and start the cron container
  block:
    - name: Ensure latest image is pulled
      community.docker.docker_image_pull:
        name: php:8.2-cli-alpine
    - name: Build the cron container
      community.docker.docker_image:
        name: backend-cron
        build:
          dockerfile: Dockerfile.cron
          path: /docker/new_eestecnet_BE
        source: build
        force_source: true

    - name: Start the cron container
      community.docker.docker_container:
        name: backend-cron
        image: backend-cron
        hostname: cron.docker
        state: started
        restart: true
        restart_policy: unless-stopped
        networks:
          - name: eestec
            aliases:
              - cron.docker
        volumes:
          - /docker/nginx/new_eestecnet_BE/:/docker/new_eestecnet_BE

- name: Build and start the queues container
  block:
    - name: Ensure latest image is pulled
      community.docker.docker_image_pull:
        name: php:8.2-cli
    - name: Build the queues container
      community.docker.docker_image:
        name: backend-queues
        build:
          dockerfile: Dockerfile.queues
          path: /docker/new_eestecnet_BE
        source: build
        force_source: true

    - name: Start the queues container
      community.docker.docker_container:
        name: backend-queues
        image: backend-queues
        hostname: queues.docker
        state: started
        restart: true
        restart_policy: unless-stopped
        networks:
          - name: eestec
            aliases:
              - queues.docker
        volumes:
          - /docker/nginx/new_eestecnet_BE/:/docker/new_eestecnet_BE
