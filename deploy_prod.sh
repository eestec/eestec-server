#!/usr/bin/env bash
ansible-playbook site.yml -i inventories/production -i credentials/production --vault-id production@.prod_password
