#!/bin/bash

ansible-playbook site.yml -i inventories/development -i credentials/development --vault-id development@.dev_password
